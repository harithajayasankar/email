package email.project;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.AuthenticationFailedException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.List;

public class MailWithAttachment {
	
	  public static void sendEmail(String from, String fpass, String to, String cc, String bcc, String subject,String message){
	        final String username = from;
	        final String password = fpass;
	        Properties props = new Properties();
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.port", "587");
	        

	     
	        Session session = Session.getInstance(props,
	                new javax.mail.Authenticator() {
	                  protected PasswordAuthentication getPasswordAuthentication() {
	                      return new PasswordAuthentication(username, password);
	                  }
	                });
	        
	       
	      Message msg = new MimeMessage(session);
	      
	      try {
	    	// compose message  
	          msg.setFrom(new InternetAddress(username));
	          InternetAddress[] toArray = InternetAddress.parse(to);
	          InternetAddress[] ccArray = InternetAddress.parse(cc);
	          InternetAddress[] bccArray = InternetAddress.parse(bcc);
	 
	          msg.setRecipients(Message.RecipientType.TO,toArray);
	          msg.addRecipients(Message.RecipientType.CC, ccArray);
	          msg.addRecipients(Message.RecipientType.BCC,bccArray);
	          msg.setSubject(subject);
	          
	          
	     
	          // create Multipart object and add MimeBodyPart objects to this object 
	          Multipart multipart = new MimeMultipart();
	          
	          //create MimeBodyPart object and set your message text 
	          MimeBodyPart textBodyPart = new MimeBodyPart();
	          textBodyPart.setText(message);
	          MimeBodyPart attachmentBodyPart= new MimeBodyPart();
	          String filename = "EmailText.txt";
	          DataSource source = new FileDataSource(filename); 
	          attachmentBodyPart.setDataHandler(new DataHandler(source));
	          attachmentBodyPart.setFileName(filename); 
	          multipart.addBodyPart(textBodyPart); 
	          multipart.addBodyPart(attachmentBodyPart); 
	          
	          //set the multiplart object to the message object  
	          msg.setContent(multipart);
	          //send message 
	          Transport.send(msg);
	          System.out.println("Done");
	      } 
	      catch (MessagingException e) {
	    	  Logger LOGGER= Logger.getLogger(MailWithAttachment.class.getName());
	          LOGGER.log(Level.SEVERE,"Error while sending email",e);
	      }
	    }
	    public static void main(String[] args)
	    {
	    }
	}





